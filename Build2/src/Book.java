public class Book {
    private int year;
    private String author;
    private String name;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    public Book(String name, int year, String author){
        this.name = name;
        this.year = year;
        this.author = author;
    }

    @Override
    public String toString() {
        return String.format("Наименование: %s Год выпуска: %d Автор: %s",name,year,author);
    }
}
