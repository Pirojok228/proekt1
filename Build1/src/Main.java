import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Student[] stud = new Student[10];
        for(int i = 0;i<10;i++){
            String fio = typeString("Фамилия");
            String group = typeString("Группа");
            double[] usp = new double[5];
            for(int j = 0;j<5;j++){
                usp[j]=Double.valueOf(typeString("Оценка"));
            }
            stud[i]=new Student(fio,group,usp);
        }
        for(int i = 0;i<9;i++){
            for(int j=i+1;j<10;j++){
                if(stud[i].getSrBall()>stud[j].getSrBall()){
                    Student temp = stud[i];
                    stud[i]=stud[j];
                    stud[j]=temp;
                }
            }
        }
        System.out.println("Все студенты");
        for(int i = 0;i<10;i++){
            System.out.println(stud[i].toString());
        }
        System.out.println("Студенты отличники и хорошисты");
        boolean canbeshowed = true;
        for(int i = 0;i<10;i++){
            for(int j=0;j<5;j++){
                if(!(stud[i].getUsp()[j]==4||stud[i].getUsp()[j]==5)){
                    canbeshowed = false;
                }
            }
            if(canbeshowed) System.out.println(stud[i].toString());
            canbeshowed = true;
        }
    }
    public static String typeString(String str){
        Scanner scan = new Scanner(System.in);
        System.out.println(str);
        return scan.next();
    }
}
